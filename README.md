# COKE MACHINE EXAMPLE #

Si implementi un programma *Java* per risolvere il problema di prelevare lattine di coca-cola da una  macchinetta e  di rifornirla nel  caso in cui rimanga vuota. 
Si utilizzi a tale scopo i **semafori** del  package `java.util.concurrent` come  meccanismo  di  sincronizzazione.

- definire  le  classi per i thread `User` e `Operator` che rispettivamente consumano e riforniscono lattine di coca-cola,
- definire la classe `CokeMachine` contenente le lattine di coca-cola (oggetti condivisi)  ed  i  metodi:
	- `consume()`, eseguito  dal  generico utente per prelevare una lattina dalla macchinetta
	- `refill()`,  eseguito  dal  fornitore  del  servizio  per  caricare  la  macchinetta  nel  caso  in  cui rimane  vuota

Si  assuma  che  inizialmente  la  macchinetta  sia piena,  e  che  un  utente  (a  scelta:  il  primo  a trovare  la  macchinetta  vuota  o  l’utente  che  preleva  l’ultima  lattina)  segnala  al  fornitore  che  la macchinetta è vuota, il quale esegue il rifornimento.