package it.unibg.so.lab3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class CokeMachine {
	
	private List<Coke> values;
	
	private int bufferSize;
	private int count;
	
	private Semaphore mutex;
	private Semaphore empty;
	private Semaphore full;	

	public CokeMachine(int items) {
		bufferSize = items;
		count = 0;
		values = new ArrayList<Coke>(bufferSize);
		
		mutex = new Semaphore(1);
		empty = new Semaphore(0);
		full = new Semaphore(bufferSize);
		
		refill();
	}
	
	public void refill(){
		try {
			full.acquire(bufferSize);
	        mutex.acquire();
	    } catch (InterruptedException e) {
	    	e.printStackTrace();
	    }
	     
		for(int i=0; i<bufferSize; i++)
			values.add(count++, new Coke(i));
	 
	    System.out.println("[Operator] machine FULL.");
	 
	    mutex.release();
	    empty.release(bufferSize);
	}

	public Coke consume() {
		try {
			empty.acquire();
	        mutex.acquire();
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
	    
		Coke item = values.remove(--count);
	 
	    if (count == 0)
	    	System.out.println("[User] consume: " + item + ", machine is EMPTY");
	    else
	        System.out.println("[User] consume: " + item + ", remaining cokes = " + count);
	 
	    mutex.release();
	    full.release();
	    return item;
	}

}
