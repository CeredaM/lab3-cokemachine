package it.unibg.so.lab3;


public class Operator extends Thread {
	private  CokeMachine cokeMachine;
	   
	public Operator(CokeMachine machine) {
		cokeMachine = machine;
	}
	
	@Override
	public void run() {
	
		while (true) {
	
			// produce tries to refill the coke machine
			System.out.println("Operator tries to refill.");
			
			cokeMachine.refill();
		}
	}
}
