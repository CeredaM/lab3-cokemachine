package it.unibg.so.lab3;

public class Coke {
	
	private int id;
	
	public Coke(int id){
		this.id = id;
	}

	@Override
	public String toString() {
		return "Coke item ID = " + id;
	}

}
